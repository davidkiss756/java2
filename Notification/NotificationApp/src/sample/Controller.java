package sample;


import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.controlsfx.control.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Controller {

    @FXML DatePicker datePicker;
    @FXML TextField dateHour, dateMin;
    @FXML TextArea textArea;

    int reminderYear;
    int reminderMonth;
    int reminderDay;
    int reminderHour;
    int reminderMinute;

    int currentYear;
    int currentMonth;
    int currentDay;
    int currentHour;
    int currentMinute;

    Executor executor = Executors.newSingleThreadExecutor();

    public int compareDates(Calendar now, Calendar comapre)
    {
        //return values
        // 1 if compareTo date is SMALLER
        // -1 if compareTo date is BIGGER
        // 0 if it is equal

        return now.compareTo(comapre); // compare to dates
    }

    public void initialize()
    {
        executor.execute(task); // run the Thread from start
    }

    //TODO end Thread on app close

    Runnable task = new Runnable() {
        @Override
        public void run() {
            try {
                while (true)
                {
                    System.out.println("THREAD RUN");
                    Thread.sleep(10000); // 10.000 == 10 seconds
                    Calendar now = Calendar.getInstance();
                    getCurrentTime();
                    now.set(currentYear, currentMonth, currentDay, currentHour, currentMinute);

                    // loop list?
                    Calendar notifyTime = Calendar.getInstance();
                    notifyTime.set(2020,12,15,18,25);

                    int callNotifyIf = compareDates(now, notifyTime);

                    if (callNotifyIf == 0)
                    {
                        notifier("Test Notification", "The message of the notiofication");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    //when to notify me
    public void setNotificationTime()
    {
        try {
            LocalDate localDate = this.datePicker.getValue();
            reminderYear = localDate.getYear();
            reminderMonth = localDate.getMonthValue();
            reminderDay = Integer.parseInt(String.valueOf(localDate.getDayOfMonth()));
            reminderHour = Integer.parseInt(dateHour.getText());
            reminderMinute = Integer.parseInt(dateMin.getText());

            System.out.println("#####Notify me on: " + reminderYear + "-" + reminderMonth + "-" + reminderDay + " time: " + reminderHour + ":" + reminderMinute);
            textArea.appendText("REMINDER ON: " + reminderYear + " " + reminderMonth + " " + reminderDay + " " + reminderHour + reminderMinute + "\n");
        }catch (Exception e){
            System.out.println("Exception: " + e);
        }
    }

    //get the current time of this moment
    public void getCurrentTime()
    {
        Date date = new Date();
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyyMMddHHmm");
        String currentDate = (sdformat.format(date));

        currentYear = Integer.parseInt(currentDate.substring(0,4));
        currentMonth = Integer.parseInt(currentDate.substring(4,6));
        currentDay = Integer.parseInt(currentDate.substring(6,8));
        currentHour = Integer.parseInt(currentDate.substring(8,10));
        currentMinute = Integer.parseInt(currentDate.substring(10,12));

        //System.out.println("Current time full: " + currentDate + "SPLIT: " + currentYear + " " + currentMonth + " " + currentDay + " " + currentHour + ":" + currentMinute);
    }

    //notify on button press
    public void sendNotification()
    {
        setNotificationTime(); // the notify time is set to
    }

    //notify function
    private static void notifier(String pTitle, String pMessage)
    {
        Platform.runLater(() -> {
                    Stage owner = new Stage(StageStyle.TRANSPARENT);
                    StackPane root = new StackPane();
                    root.setStyle("-fx-background-color: TRANSPARENT");
                    Scene scene = new Scene(root, 1, 1);
                    scene.setFill(Color.TRANSPARENT);
                    owner.setScene(scene);
                    owner.setWidth(1);
                    owner.setHeight(1);
                    owner.toBack();
                    owner.show();
                    Notifications.create().title(pTitle).text(pMessage).showInformation();
                }
        );
    }
}
