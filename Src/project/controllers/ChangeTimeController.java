package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class ChangeTimeController {
    @FXML TextField dateHour, dateMinute;
    @FXML DatePicker datePicker;

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    private int year, month, day, hour, minute;

    @FXML public void addSelectedTimeBtn(ActionEvent event)
    {
        LocalDate localDate = this.datePicker.getValue();
        year = localDate.getYear();
        //String month = String.valueOf(localDate.getMonth());
        month = localDate.getMonthValue();
        day = Integer.parseInt(String.valueOf(localDate.getDayOfMonth()));
        hour = Integer.parseInt(dateHour.getText());
        minute = Integer.parseInt(dateMinute.getText());
    }
}
