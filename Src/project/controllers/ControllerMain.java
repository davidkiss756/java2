package project.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.collections.ObservableList;
import project.*;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;

public class ControllerMain {

    LocalDateTime currentDate;
    ObservableList<dayTableModel> dayData = FXCollections.observableArrayList();
    ObservableList<weekTableModel> weekData = FXCollections.observableArrayList();
    ObservableList<monthTableModel> monthData = FXCollections.observableArrayList();

    @FXML private AnchorPane anchorPane;

    @FXML private TableView<dayTableModel> dayTable;
    @FXML private TableColumn<dayTableModel, Integer> dayCol_hour;
    @FXML private TableColumn<dayTableModel, Integer> dayCol_min;
    @FXML private TableColumn<dayTableModel, String> dayCol_title;
    @FXML private TableColumn<dayTableModel, String> dayCol_category;
    @FXML private TableColumn<dayTableModel, Integer> dayCol_priority;

    @FXML private TableView<weekTableModel> weekTable;
    @FXML private TableColumn<weekTableModel, Integer> weekCol_day;
    @FXML private TableColumn<weekTableModel, Integer> weekCol_hour;
    @FXML private TableColumn<weekTableModel, Integer> weekCol_min;
    @FXML private TableColumn<weekTableModel, String> weekCol_title;
    @FXML private TableColumn<weekTableModel, String> weekCol_category;
    @FXML private TableColumn<weekTableModel, Integer> weekCol_priority;

    @FXML private TableView<monthTableModel> monthTable;
    @FXML private TableColumn<monthTableModel, Integer> monthCol_day;
    @FXML private TableColumn<monthTableModel, Integer>  monthCol_hour;
    @FXML private TableColumn<monthTableModel, Integer>  monthCol_min;
    @FXML private TableColumn<monthTableModel, String>  monthCol_title;
    @FXML private TableColumn<monthTableModel, String>  monthCol_category;
    @FXML private TableColumn<monthTableModel, Integer>  monthCol_priority;

    @FXML private Text current;
    @FXML private Text selected;

    @FXML
    public void initialize() {
        //create db maybe here
        System.out.println("init");
        currentDate = LocalDateTime.now();
        DB db = DB.getInstance();
        db.connect();

        dayCol_hour.setCellValueFactory(new PropertyValueFactory<>("hour"));
        dayCol_min.setCellValueFactory(new PropertyValueFactory<>("min"));
        dayCol_title.setCellValueFactory(new PropertyValueFactory<>("title"));
        dayCol_category.setCellValueFactory(new PropertyValueFactory<>("category"));
        dayCol_priority.setCellValueFactory(new PropertyValueFactory<>("priority"));

        weekCol_day.setCellValueFactory(new PropertyValueFactory<>("day"));
        weekCol_hour.setCellValueFactory(new PropertyValueFactory<>("hour"));
        weekCol_min.setCellValueFactory(new PropertyValueFactory<>("min"));
        weekCol_title.setCellValueFactory(new PropertyValueFactory<>("title"));
        weekCol_category.setCellValueFactory(new PropertyValueFactory<>("category"));
        weekCol_priority.setCellValueFactory(new PropertyValueFactory<>("priority"));

        monthCol_day.setCellValueFactory(new PropertyValueFactory<>("day"));
        monthCol_hour.setCellValueFactory(new PropertyValueFactory<>("hour"));
        monthCol_min.setCellValueFactory(new PropertyValueFactory<>("min"));
        monthCol_title.setCellValueFactory(new PropertyValueFactory<>("title"));
        monthCol_category.setCellValueFactory(new PropertyValueFactory<>("category"));
        monthCol_priority.setCellValueFactory(new PropertyValueFactory<>("priority"));

        updateDayTable();
        updateWeekTable();
        updateMonthTable();

        Notification notification = new Notification();
        notification.callExecutor();

        Thread timerThread = new Thread(() -> {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            while (true) {
                try {
                    Thread.sleep(1000); //1 second
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                final String time = simpleDateFormat.format(new Date());
                Platform.runLater(() -> {
                    current.setText("Current time: " + time);
                    updateDayTable();
                    updateWeekTable();
                    updateMonthTable();
                });
            }
        });
        timerThread.start();
    }

    // public no args constructor
    public ControllerMain()
    {

    }

    public void updateDayTable()
    {
        String sqlQuery = "SELECT * FROM todos where day='" + currentDate.getDayOfMonth() + "' ORDER BY priority asc";
        ResultSet results = DB.getInstance().query(sqlQuery);
        dayTable.getItems().clear();
        if (results != null)
        {
            try{
                while(results.next()){
                    dayData.add(new dayTableModel(results.getInt("id"),results.getInt("hour"),results.getInt("min"),results.getString("title"),results.getString("desc"),results.getString("category"),results.getInt("priority")));
                }
                dayTable.setItems(dayData);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void updateWeekTable()
    {
        String sqlQuery = "SELECT * FROM todos where day BETWEEN '" + (currentDate.getDayOfMonth()-currentDate.getDayOfWeek().getValue()) + "' AND '" + ((currentDate.getDayOfMonth()-currentDate.getDayOfWeek().getValue())+6) + "' ORDER BY priority asc";
        ResultSet results = DB.getInstance().query(sqlQuery);
        weekTable.getItems().clear();
        if (results != null)
        {
            try{
                while(results.next()){
                    weekData.add(new weekTableModel(results.getInt("id"),results.getInt("day"),results.getInt("hour"),results.getInt("min"),results.getString("title"),results.getString("desc"),results.getString("category"),results.getInt("priority")));
                }
                weekTable.setItems(weekData);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void updateMonthTable(){
        String sqlQuery = "SELECT * FROM todos where month='" + currentDate.getMonthValue() + "' ORDER BY priority asc";
        ResultSet results = DB.getInstance().query(sqlQuery);
        monthTable.getItems().clear();
        if (results != null)
        {
            try{
                while(results.next()){
                    monthData.add(new monthTableModel(results.getInt("id"),results.getInt("day"),results.getInt("hour"),results.getInt("min"),results.getString("title"),results.getString("desc"),results.getString("category"),results.getInt("priority")));
                }
                monthTable.setItems(monthData);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void addItemClick()
    {
        try {
            System.out.println("add button clicked");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/addTask.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Add Task");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @FXML
    public void aboutClick()
    {
        try {
            System.out.println("about button clicked");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/aboutTask.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("About");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @FXML
    public void timeChangeClick()
    {
        try {
            System.out.println("timechange button clicked");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/changeTimeTask.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Timechange");
            stage.setScene(new Scene(root));
            stage.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void closeProgram()
    {
        Stage stage = (Stage) anchorPane.getScene().getWindow();
        stage.close();
        //AlertBox.showAnAlertBox("Clos application", "Are you sure?");

/*        try {
            Stage stage = (Stage) anchorPane.getScene().getWindow();
            stage.close();
        } catch(Exception e) {
            System.out.println("Exception: " + e);
        }*/

    }
} //ControllerMain
