package project.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.InputEvent;
import javafx.stage.Stage;
import javafx.stage.Window;
import project.DB;

import java.time.LocalDate;

public class AddTaskController {

    @FXML TextField taskName, taskPriority, taskCategory, dateHour, dateMinute;
    @FXML TextArea taskDescription;
    @FXML DatePicker datePicker;
    DB db = DB.getInstance();


    @FXML public void addTasktoDatabaseBtn(ActionEvent event)
    {
        try {
            String taskName = this.taskName.getText();
            String taskDescription = this.taskDescription.getText();
            String taskCategory = this.taskCategory.getText();
            int  taskPriority = Integer.parseInt(this.taskPriority.getText());

            LocalDate localDate = this.datePicker.getValue();
            int year = localDate.getYear();
            //String month = String.valueOf(localDate.getMonth());
            int month = localDate.getMonthValue();
            int day = Integer.parseInt(String.valueOf(localDate.getDayOfMonth()));
            int hour = Integer.parseInt(dateHour.getText());
            int minute = Integer.parseInt(dateMinute.getText());

            System.out.println("taskname: " + taskName + " Desc: " + taskDescription + "cat: " + taskCategory + " protity: " + taskPriority);
            System.out.println("Date:" + " " + year + " " + month  + " " + day + " " + hour + ":" + minute);

            //add to Database
            db.insert(year, month, day, hour, minute, taskName, taskDescription, taskCategory, taskPriority);

        }catch (Exception e) {
            System.out.println("This exception: " + e);
        }finally {
            ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
        }


    }
}
