package project;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.controlsfx.control.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Notification {

    int currentYear;
    int currentMonth;
    int currentDay;
    int currentHour;
    int currentMinute;

    Map<Integer,LocalDateTime> reminderTimes = new HashMap<>();
    DB db = DB.getInstance();

    Executor executor = Executors.newSingleThreadExecutor();

    public void callExecutor()
    {
        executor.execute(task); // run the Thread from start
    }

    Runnable task  = new Runnable() {
        @Override
        public void run() {
            try {
                while (true)
                {
                    System.out.println("Thread is running bla bla");
                    Calendar now  = Calendar.getInstance();
                    getCurrentTime(); // update every minute
                    now.set(currentYear, currentMonth, currentDay, currentHour, currentMinute);

                    getRemindersNotification();
                    Calendar notifyTime = Calendar.getInstance();
                    for (Map.Entry<Integer, LocalDateTime> me : reminderTimes.entrySet())
                    {
                        notifyTime.set(me.getValue().getYear(), me.getValue().getMonthValue(), me.getValue().getDayOfMonth(),
                                me.getValue().getHour(), me.getValue().getMinute());

                        //notifyTime.set(2020,12,15, 19,45);
                        int callNotifyIf = compareDates(now, notifyTime);

                        if (callNotifyIf == 0)
                        {
                            String sqlQuery = "select title,desc from todos where id='" + me.getKey() + "'";
                            ResultSet result = db.query(sqlQuery);
                            notifier("Reminder: " + result.getString("title"), result.getString("desc"));
                            sqlQuery = "delete from todos where id='" + me.getKey() + "'";
                            PreparedStatement stmt = db.conn.prepareStatement(sqlQuery);
                            stmt.executeUpdate();
                        }
                    }

                    Thread.sleep(30000); // 0,5 minute
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void getRemindersNotification(){
        String sqlQuery = "select id,year,month,day,hour,min from todos";
        ResultSet results = db.query(sqlQuery);
        if (results != null){
            try{
                while (results.next()){
                    int reminderID = results.getInt("id");
                    int reminderYear = results.getInt("year");
                    int reminderMonth = results.getInt("month");
                    int reminderDay = results.getInt("day");
                    int reminderHour = results.getInt("hour");
                    int reminderMinute = results.getInt("min");

                    LocalDateTime date = LocalDateTime.of(reminderYear,reminderMonth,reminderDay,reminderHour,reminderMinute);
                    reminderTimes.put(reminderID,date);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    public int compareDates(Calendar now, Calendar comapre)
    {
        //return values
        // 1 if compareTo date is SMALLER
        // -1 if compareTo date is BIGGER
        // 0 if it is equal

        return now.compareTo(comapre); // compare to dates
    }


    //get the current time of this moment
    public void getCurrentTime()
    {
        Date date = new Date();
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyyMMddHHmm");
        String currentDate = (sdformat.format(date));

        currentYear = Integer.parseInt(currentDate.substring(0,4));
        currentMonth = Integer.parseInt(currentDate.substring(4,6));
        currentDay = Integer.parseInt(currentDate.substring(6,8));
        currentHour = Integer.parseInt(currentDate.substring(8,10));
        currentMinute = Integer.parseInt(currentDate.substring(10,12));

        //System.out.println("Current time full: " + currentDate + "SPLIT: " + currentYear + " " + currentMonth + " " + currentDay + " " + currentHour + ":" + currentMinute);
    }

    //notify function
    private static void notifier(String pTitle, String pMessage)
    {
        Platform.runLater(() -> {
                    Stage owner = new Stage(StageStyle.TRANSPARENT);
                    StackPane root = new StackPane();
                    root.setStyle("-fx-background-color: TRANSPARENT");
                    Scene scene = new Scene(root, 1, 1);
                    scene.setFill(Color.TRANSPARENT);
                    owner.setScene(scene);
                    owner.setWidth(1);
                    owner.setHeight(1);
                    owner.toBack();
                    owner.show();
                    Notifications.create().title(pTitle).text(pMessage).showInformation();
                }
        );
    }

} /*Notification*/
