package project;

public class weekTableModel {
    private  int id;
    private String day;
    private int hour;
    private int min;
    private String title;
    private String description;
    private String category;
    private int priority;

    public weekTableModel(int id,int day,int hour,int min, String title, String description,String category,int priority) {
        this.id = id;
        switch (day%7){
            case 0: this.day = "Monday";
                break;
            case 1: this.day = "Tuesday";
                break;
            case 2: this.day = "Wednesday";
                break;
            case 3: this.day = "Thursday";
                break;
            case 4: this.day = "Friday";
                break;
            case 5: this.day = "Saturday";
                break;
            case 6: this.day = "Sunday";
                break;
        }
        this.hour = hour;
        this.min = min;
        this.title = title;
        this.description = description;
        this.category = category;
        this.priority = priority;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }


    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
