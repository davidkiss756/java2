package project;

public class dayTableModel {
    private int id;
    private int hour;
    private int min;
    private String title;
    private String description;
    private String category;
    private int priority;

    public dayTableModel(int id,int hour,int min, String title, String description,String category,int priority) {
        this.id = id;
        this.hour = hour;
        this.min = min;
        this.title = title;
        this.description = description;
        this.category = category;
        this.priority = priority;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
