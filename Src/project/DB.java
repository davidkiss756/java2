package project;

import java.sql.*;

public class DB {

    String database = "jdbc:sqlite:database.db";
    Connection conn = null;

    private static DB instance = null;

    private DB() {

    }

    ;

    public static DB getInstance() {
        if (instance == null)
            instance = new DB();

        return instance;
    }

    public void connect() {

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(database);

            if (conn != null) {
                System.out.println("DB CONNECTION SUCCESS");
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insert(int year, int month, int day, int hour, int min, String title, String desc, String category, int priority) {
        String sqlQuery = "INSERT INTO todos(year,month,day,hour,min,title,desc,category,priority) VALUES(?,?,?,?,?,?,?,?,?)";

        if (conn != null) {
            try {
                PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
                pstmt.setInt(1, year);
                pstmt.setInt(2, month);
                pstmt.setInt(3, day);
                pstmt.setInt(4, hour);
                pstmt.setInt(5, min);
                pstmt.setString(6, title);
                pstmt.setString(7, desc);
                pstmt.setString(8, category);
                pstmt.setInt(9, priority);
                pstmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            {

            }
        }
    }

    public ResultSet query(String sqlQuery)
    {
        ResultSet rs = null;
        if (conn != null && sqlQuery != null){
            try {
                rs = conn.createStatement().executeQuery(sqlQuery);
                return  rs;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rs;
    }

}
